<!DOCTYPE html>
<html lang="en">
<head>
<title>CSS Template</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

header {
  background-color: #964B00;
  padding: 30px;
  text-align: center;
  font-size: 35px;
  color: white;
}


nav {
  float: left;
  width: 30%;
  height: 300px; /* only for demonstration, should be removed */
  background: #964;
  padding: 20px;
}

/* Style the list inside the menu */
nav ul {
  list-style-type: none;
  padding: 0;
}

article {
  float: left;
  padding: 20px;
  width: 70%;
  background-color: #00FFFF;
  height: 300px; /* only for demonstration, should be removed */
}

/* Clear floats after the columns */
section::after {
  content: "";
  display: table;
  clear: both;
}

/* Style the footer */
footer {
  background-color: red;
  padding: 10px;
  text-align: center;
  color: white;
}

/* Responsive layout - makes the two columns/boxes stack on top of each other instead of next to each other, on small screens */
@media (max-width: 600px) {
  nav, article {
    width: 100%;
    height: auto;
  }
}
</style>
</head>
<body>

<header>
  <center><img width="1155px" height="200px" align="middle" style="Margin-left: 10px;" src="Gambar Website.png" />
</header>

<section>
  <nav>
    <ul>
      <li><button><a href="#">HOME</a></button></li>
	  <br>
      <li><button><a href="#">PEMESANAN</a></button></li>
      <br>
	  <li><button><a href="#">ABOUT</a></button></li>
	</ul>
  </nav>
  
  <article>
    <h1>HOME</h1>
    <p>Selamat Datang di Website Pemesanan Obat Herbal</p>
    <p>menyediakan segala obat herbat untuk menjadi saranan pengobatan bagi Masyarakat, cepat efisien dan aman</p>
  </article>
</section>

<footer>
  <p>Copyrigth@2021</p>
</footer>

</body>
</html>
